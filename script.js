const inputSearch = document.getElementById('inputSearch');
const randomMeal = document.getElementById('randomMeal');
const results = document.getElementById('results');


//let search = "";
const fetchSearch = async(url) => {
    meals = await fetch(
       /* `https://www.themealdb.com/api/json/v1/1/search.php?s=${search}`*/
       `https://www.themealdb.com/api/json/v1/1/${url}`)
        .then(res => res.json())
        .then(res => res.meals)
        //console.log(meals);
}

//INPUT SEARCH
const searchDisplay = async() => {
    await fetchSearch(search);
    if(meals == null){
        results.innerHTML = '<span class="noResult"> Aucun résultat </span>';
    }else{
        results.innerHTML = (
            meals.map(meal => (
            `<div class="searchBox">
                <h2>${meal.strMeal}</h2>
                <div class="description">
                <div>origine : ${meal.strArea}</div>
                <div>catégorie : ${meal.strCategory}</div>
                </div>
                <img src='${meal.strMealThumb}' /></br>
                <a href="${meal.strYoutube}" target="_blank"><i class="fab fa-youtube"></i></a>
            </div>`
            )).join('')
        );
    };

};

inputSearch.addEventListener('input', (event) => {
    //console.log(event.target.value);
    //search = event.target.value;
    search = `search.php?s=${event.target.value}`;

    searchDisplay();
})

//RANDOM MEALS
const randomDisplay = async() => {
    await fetchSearch('random.php');

    results.innerHTML = (
        meals.map(meal => (
            `<div class="randomContainer">
                <h2>${meal.strMeal}</h2>
                <div class="description">
                <div>origine : ${meal.strArea}</div>
                <div>catégorie : ${meal.strCategory}</div>
                </div>
                <img src='${meal.strMealThumb}' /></br>
                <p>${meal.strInstructions}</p>
                <a href="${meal.strYoutube}" target="_blank"><i class="fab fa-youtube"></i></a>
             </div>`
        ))
    );
};
 
randomMeal.addEventListener('click',randomDisplay);
// Pour le choix aléatoire s'afficher dans le site initial
randomDisplay();